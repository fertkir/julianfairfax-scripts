#!/bin/bash

for url in $(flatpak list --columns=origin,ref | sed 's|/.*||' | sed 's|.*\t|https://flathub.org/apps/|'); do
	content=$(curl -s $url)

	if [[ -z $(echo $content | grep "aria-label=\"This app is verified\"") && -z $(echo $content | grep "404 - Page not found") && -z $(echo $content | grep "500 - An error occurred on the server.") ]]; then
		echo "$url is not verified"
	fi
done
