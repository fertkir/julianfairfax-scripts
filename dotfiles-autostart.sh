#!/bin/bash

if [[ ! -d ~/.config/autostart ]]; then
	mkdir ~/.config/autostart
fi

if [[ -f /var/lib/flatpak/exports/share/applications/ch.protonmail.protonmail-bridge.desktop && ! -f ~/.config/autostart/ch.protonmail.protonmail-bridge.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=ch.protonmail.protonmail-bridge
Exec=flatpak run ch.protonmail.protonmail-bridge --no-window
X-Flatpak=ch.protonmail.protonmail-bridge" | tee ~/.config/autostart/ch.protonmail.protonmail-bridge.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/com.nextcloud.desktopclient.nextcloud.desktop && ! -f ~/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=Nextcloud
Exec=flatpak run com.nextcloud.desktopclient.nextcloud --background
Icon=Nextcloud" | tee ~/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/de.schmidhuberj.Flare.desktop && ! -f ~/.config/autostart/de.schmidhuberj.Flare.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=de.schmidhuberj.Flare
Exec=flatpak run de.schmidhuberj.Flare --gapplication-service
X-Flatpak=de.schmidhuberj.Flare" | tee ~/.config/autostart/de.schmidhuberj.Flare.desktop
fi

# if [[ -f /var/lib/flatpak/exports/share/applications/dev.geopjr.Tuba.desktop && ! -f ~/.config/autostart/dev.geopjr.Tuba.desktop ]]; then
# 	echo "[Desktop Entry]
# Type=Application
# Name=dev.geopjr.Tuba
# Exec=flatpak run dev.geopjr.Tuba --hidden
# X-Flatpak=dev.geopjr.Tuba" | tee ~/.config/autostart/dev.geopjr.Tuba.desktop
# fi

# if [[ -f /var/lib/flatpak/exports/share/applications/im.riot.Riot.desktop && ! -f ~/.config/autostart/im.riot.Riot.desktop ]]; then
# 	echo "[Desktop Entry]
# Type=Application
# Name=im.riot.Riot
# Exec=flatpak run im.riot.Riot --hidden
# X-Flatpak=im.riot.Riot" | tee ~/.config/autostart/im.riot.Riot.desktop
# fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Calls.desktop && ! -f ~/.config/autostart/org.gnome.Calls.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=org.gnome.Calls
Exec=flatpak run org.gnome.Calls --daemon
X-Flatpak=org.gnome.Calls" | tee ~/.config/autostart/org.gnome.Calls.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Geary.desktop && ! -f ~/.config/autostart/org.gnome.Geary.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=org.gnome.Geary
Exec=flatpak run --gapplication-service
X-Flatpak=org.gnome.Geary" | tee ~/.config/autostart/org.gnome.Geary.desktop
fi

# if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop && ! -f ~/.config/autostart/org.signal.Signal.desktop ]]; then
# 	echo "[Desktop Entry]
# Type=Application
# Name=org.signal.Signal
# Exec=SIGNAL_START_IN_TRAY=1 flatpak run org.signal.Signal
# X-Flatpak=org.signal.Signal" | tee ~/.config/autostart/org.signal.Signal.desktop
# fi

if [[ -f /var/lib/flatpak/exports/share/applications/sm.puri.Chatty.desktop && ! -f ~/.config/autostart/sm.puri.Chatty.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=sm.puri.Chatty
Exec=flatpak run sm.puri.Chatty --daemon
X-Flatpak=sm.puri.Chatty" | tee ~/.config/autostart/sm.puri.Chatty.desktop
fi

if [[ -f /etc/xdg/autostart/knmd.desktop && ! -f ~/.config/autostart/knmd.desktop ]]; then
	cp /etc/xdg/autostart/knmd.desktop .config/autostart/
	echo "Hidden=true" | tee -a .config/autostart/knmd.desktop
fi
