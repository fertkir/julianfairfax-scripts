#!/bin/bash

if [[ -f /usr/lib/systemd/user/proton-bridge.service ]]; then
	systemctl enable --user --now proton-bridge.service
fi

if [[ -f ~/.config/emailproxy.config ]]; then
	systemctl enable --user --now email-oauth2-proxy@$USER.service
fi

systemctl enable --user --now syncthing.service
